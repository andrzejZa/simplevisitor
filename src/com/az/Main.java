package com.az;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Visitable> shapes = new ArrayList<>();
        shapes.add( new Triangle(2,3,4));
        shapes.add(new Sphere(5));
        shapes.add(new Rectangle(6,7));

        Printer p = new Printer();

        for(Visitable item: shapes) {
            item.accept(p);
        }
    }


}

interface Visitable{
    public void accept(Visitor visitor);
}

interface Visitor{
    public void visit(Triangle triangle);

    public void visit(Rectangle rectangle);
    public void visit(Sphere sphere);
}

class Printer implements Visitor{
    @Override
    public void visit(Triangle triangle) {
        System.out.println("This is triangle");
        System.out.println(triangle.sideA+" "+ triangle.sideB+" "+  triangle.sideC);
    }

    @Override
    public void visit(Rectangle rectangle) {
        System.out.println("This is rectangle");
        System.out.println(rectangle.sideA+" "+ rectangle.sideB);
    }

    @Override
    public void visit(Sphere sphere) {
        System.out.println("This is sphere");
        System.out.println(sphere.radius);
    }
}

class Triangle implements   Visitable{
    int sideA;
    int sideB;
    int sideC;
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public Triangle(int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }
}

class Rectangle implements  Visitable{
    int sideA;
    int sideB;
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public Rectangle(int sideA, int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }
}

class Sphere implements  Visitable{
    int radius;
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public Sphere(int radius) {
        this.radius = radius;
    }
}
